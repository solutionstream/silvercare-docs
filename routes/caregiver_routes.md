# CareGiver Endpoints

<!-- CREATE CAREGIVER  -->
{% method -%}
## Create CareGiver

Creating a CareGiver for a user account enables the user to act as a caregiver in the application.  Adding the caregiver to a patient's carecircle is an additional step.

### Endpoint
`PUT /user/:user_id/caregiver/:caregiver_id`

Param         | Description
------------- | -----------
:user_id      | id for existing user account
:caregiver_id | id created by client

### Access Control
- PAC Admin (caregiver)
- SV Admin

### Body
Param     | Description
--------- | -----------
full_name | Full name of the CareGiver **(required)**
pac_id    | The ID of the PAC that the new caregiver should be associated with.  Set automatically if the user is a PAC Admin.
address   | Contact address for caregiver
phone     | Contact phone number for caregiver
email     | Contact email address for caregiver

### Response Codes
Code | Description
---- | -----------
201  | Journal Created
400  | Invalid Body
403  | Insufficient Permissions
404  | User not found

{% sample lang="http" -%}
Request
```curl
PUT /patient/598b50c5b951971298e08dd5/journal/598df3993773bac262c83839
```

Body
```json
{
	"title": "Journal Title",
	"body": "Journal Note Body"
}
```

Response
```json
{
    "_id": "598df3993773bac262c83839",
    "patient_id": "598b50c5b951971298e08dd5",
    "type": "journal",
    "title": "Journal Title",
    "author": {
        "_id": "598b50c5b951971298e08dd5",
        "type": "patient",
        "full_name": "Patient Zero"
    },
    "meta": {
        "body": "Journal Note Body"
    },
    "created": 1502475209896,
    "updated": 1502475209896
}
```

{% endmethod %}