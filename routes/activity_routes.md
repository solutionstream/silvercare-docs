# Activity Feed Endpoints

<!-- CREATE JOURNAL  -->
{% method -%}
## Create Journal

A journal is a free-entry note that can be added to a patient's activitylog by members of a patient's carecircle.

### Endpoint
`PUT /patient/:patient_id/journal/:journal_id`

Param       | Description
----------- | -----------
:patient_id | id for existing patient
:journal_id | id created by client

### Access Control
- CareGiver (patient)

### Body
Param | Description
----- | -----------
title | The journal title
body  | The journal content

### Response Codes
Code | Description
---- | -----------
201  | Journal Created
400  | Invalid Body
403  | Insufficient Permissions
404  | Patient not found

{% sample lang="http" -%}
Request
```curl
PUT /patient/598b50c5b951971298e08dd5/journal/598df3993773bac262c83839
```

Body
```json
{
	"title": "Journal Title",
	"body": "Journal Note Body"
}
```

Response
```json
{
    "_id": "598df3993773bac262c83839",
    "patient_id": "598b50c5b951971298e08dd5",
    "type": "journal",
    "title": "Journal Title",
    "author": {
        "_id": "598b50c5b951971298e08dd5",
        "type": "patient",
        "full_name": "Patient Zero"
    },
    "meta": {
        "body": "Journal Note Body"
    },
    "created": 1502475209896,
    "updated": 1502475209896
}
```

{% endmethod %}

<!-- SEARCH ACTIVITIES  -->
{% method -%}
## Search Activities

Search the patient's activityfeed

### Endpoint
`GET /patient/:patient_id/activities`

Param       | Description
----------- | -----------
:patient_id | id for existing patient

### Body
Param     | Description
--------- | -----------
type      | The type of activity
term      | Search term for use with FTS (currently only journals)
start     | Only match activities created past this date **(required)**
until     | Only match activities created before this date
limit     | Limit the number of results
author_id | Only match activities created by this author

### Access Control
- CareGiver (patient)
- PAC Admin (patient)
- SV Admin

### Response Codes
Code | Description
---- | -----------
400  | Invalid Body
403  | Insufficient Permissions
404  | Patient not found

{% sample lang="http" -%}
Request
```curl
GET localhost:8000/patient/598b50c5b951971298e08dd5/activities?type=journal&start=1502302415591&until=1502477915207&limit=1&author_id=598b50c5b951971298e08dd5&term=Note
```

Response
```json
{
    "activities": [
        {
            "_id": "598df3993773bac262c83839",
            "patient_id": "598b50c5b951971298e08dd5",
            "type": "journal",
            "title": "Journal Title",
            "author": {
                "_id": "598b50c5b951971298e08dd5",
                "type": "patient",
                "full_name": "Patient Zero"
            },
            "meta": {
                "body": "Journal Note Body"
            },
            "created": 1502475209896,
            "updated": 1502475209896
        }
    ]
}
```

{% endmethod %}