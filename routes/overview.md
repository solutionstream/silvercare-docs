## Headers
- **Content-Type**: "application/json"
- **Authorization**: "Basic <token>" | "Bearer <token>"

## Authorization
The server supports basic and token authentication.  The `Authorization` header must be present on every request.
Token authentication is mostly used when emailing a link to a user.

## Requests

### Dates
Dates in requests and responses are represented as the number of milliseconds since epoch e.g., `1502475209896`

### JSON
The `Content-Type` header must be set to `"application/json"` on every request with a content body and the body must be JSON.

### URLs

- All database ids are `BSON ObjectID` strings which are 24-bit hex strings.
- When creating new entities the client is expected to provide the id to use
- In some endpoints the server is able to determine the id by providing special id params e.g., `me` or `profile`.

## Responses
The API always returns JSON.

### Response Codes
Code | Description
---- | -----------
200  | The request completed successfully
201  | The requested entity was created successfully
204  | The request completed succesfully but no response body is set
400  | The server received bad request parameters.  Either required fields are missing, extra fields were included, or a value failed validation.
401  | The authorization header contained invalid credentials
403  | Authorization was successful but the user has insufficient access to the endpoint
404  | One of the entities being acted upon does not exist in the database+
409  | A change is requested that conflicts with the state of the database
500  | Probably a database constraint issue

### Errors
When an error occurs the server will include an application error code in the JSON body.
This provides application specific context for what went wrong, the codes vary by endpoint.

```json
{
    "error": "Application Error Code"
}
```
