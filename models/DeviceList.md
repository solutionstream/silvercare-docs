# DeviceList

## Devices

A Device is simply an endpoint where reminders/alerts are to be sent.  It can be one of `ios`, `android`, `sms`, `email`, `pers`.

`ios` and `android` refer to installations of the mobile app.  Reminders/Alerts are sent via push notificaiton.

`sms` refers to SMS capable mobile numbers.  Reminders/Alerts are sent via SMS/MMS.

`email` refers to email addresses.  Reminders/Alerts are sent via email.

`pers` refers to PERS devices.  Reminders/Alerts are sent via the FREEUS API.

## DeviceList

The devicelist is a heterogeneous collection of device objects.  A `User` may have zero or one devicelists.

```ts
class DeviceList implements IDeviceList {
    _id: database.ObjectID;
    user_id: database.ObjectID;
    version: string;
    devices: Device[];
```

Since it is necessary at times to communicate with multiple users we are able to "merge" multiple device lists into a single one.  The single devicelist will be an aggregate of the devices contained in each subordinate devicelist.

```ts
    // Create one devicelist from multiple
    static merge(devicelists: DeviceList[]): DeviceList;
```

This sends a push notification to all of the push capable devices (`ios` and `android`) in a devicelist.

```ts
    async push(options: DeviceList.PushOptions): Promise<void>;
```

This sends an SMS/MMS message to all of the SMS capable devices (`sms`) in a devicelist.

```ts
    async sms(options: DeviceList.SMSOptions): Promise<void>;
```

This sends an email to all of the email capable devices (`email`) in a devicelist.
```ts
    async email(options: DeviceList.EmailOptions): Promise<void>
```

This sends a command to all of the PERS devices (`pers`) in a devicelist.
```ts
    async pers(options: DeviceList.PERSOptions): Promise<void>
```