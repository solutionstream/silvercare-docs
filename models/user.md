# User

A `User` object maps directly to an account.  The user holds information required for authentication including username, password, tokens, lockout info, and roles.

## Password Encryption

Passwords are salted and hashed using bcrypt.  The number of rounds when generating a salt is configurable in the `.env` file.

We always check that passwords are encrypted prior to writing a user object to the database.  The password salt is unique for every user to ensure that two users with the same plaintext password are written as different hashes.

```ts
export declare class Password {
    // Determine if a string is already encrytped
    static isCiphertext(ciphertext: string): boolean;

    // Generate a unique salt
    static generateSalt(): Promise<string>;

    // Hash a plaintext string using the provided salt
    static hash(password: string, salt: string): Promise<string>;

    // Salt and hash a plaintext password.  Returns ciphertext.
    static encrypt(password: string): Promise<string>;

    // Compare a plaintext password to a ciphertext to determine whether they match
    static compare(plaintext: string, ciphertext: string): Promise<boolean>;
}
```

## Authentication

A `User` can be authenticated by using either:
- username + password
- username + token id

### Failed Logins

When a user fails to authenticate the `failed_login` counter is incremented.
When this counter exceeds the configured maximum (`.env`) the account is "locked" for the configured number of minutes.

Any attempt to authenticate during the lockout period (even with correct credentials) is ignored.  The lockout period is not extended with additional tries.

The lockout is lifted when the user authenticates successfully AND the lockout period has elapsed.

### JWT

The User model can issue authentication tokens.  The tokens are signed with a configurable secret phrase (`.env`).

Because tokens are password equivalent we do not store the token text in the database.  Instead we store the unique token id.  A token using that id cannot be forged since it would require access to the session secret.

```ts
interface IssueTokenOptions {
    // The unique identifier for the token
    // This allows us to differentiate one issued token from another
    id?: string;

    // TTL of the token in seconds
    // Tokens that have expired are automatically revoked when used
    expires?: number;

    // If true the token is automatically revoked after use
    once?: boolean;
}
```

## User Model
```ts
class User implements IUser {
    // Unique identifier for a user object
    _id: database.ObjectID;

    // Document version in the database
    // Ensures that we are dealing with the latest version prior to writing to DB
    version: string;

    // Roles determine any special permissions given to the user e.g., sv_admin, sv_super, pac_admin
    roles: User.Role[];
    
    // An array of token ids that have been issued to this user
    tokens: string[];

    // Username that identifies the user
    // Unique across users
    username: string;

    // Encrypted password
    // When the password is `null` the user has not been setup
    password: null | string;

    // Counter for the number of failed authentication attempts
    failed_logins: number;

    // The Date until the user account is unlocked
    locked_until: null | Date;

    // Whether the user has accepted the Terms and Conditions of Service
    read_tacos: boolean;
}
```
