# TaskList

A `Tasklist` is an object capable of deriving zero or more `Task Instances` by applying `Exceptions` to zero or more `Schedules`.

## Schedules

A schedule is an object that describes rules for determining which datetimes contain a `Task Instance`.  Schedules are a subset of the `rrule` standard with a few custom additions.

```ts
class Schedule {
```

The frequency of a schedule is like the "unit" of time that will be applied to dates.  These can be one of `"hourly"`, `"daily"`, `"weekly"`, and `"monthly"`.

```ts
    frequency: "hourly"|"daily"|"weekly"|"monthly";
```

The start date is the date at which schedule calculation begin.  No instances will occur prior to this date.  The start date may or may not be an instance.

```ts
    start: Date;
```

The until date is the date at which schedule calculations end.  No instances will occur beyond this date.  The until date may or may not be an instance.

```ts
    until: Date;
```

The days of month filter is applied to calculated instances to determine whether to include them.  The values are the numeric days of the month 1-31 where an instance may occur.  Leaving the array empty means that all days are allowed, adding values to the array means that only instances ocurring on that day of month are considered e.g., only on the first of the month.

```ts
    days_of_month?: number[];
```

The days of week filter is applied to calculated instances to determine whether to include them.  The values are any combination of `"mon"`, `"tue"`, `"wed"`, `"thu"`, `"fri"`, `"sat"`, `"sun"`.  Leaving the array empty means that all days are allowed; adding values to the array means that only instances occuring on that day of week are considered e.g., only weekends.

```ts
    days_of_week?: Schedule.Weekday[];
```

A task can be scheduled to ocurr multiple times in one day e.g., medication that must be taken at 10AM, 2PM, and 6PM.  The times in minutes array is the number of minutes relative to the start of day.  Leaving the array empty means that each instance occurs at its regular time; adding values to the array means that each schedule instance will be duplicated and shifted to ocurr at the minutes scheduled.

For example, a daily medication task would normally ocurr once per day.  However, if `times_in_minutes` is specified for `[600, 840]` the medication would instead occur at 10am and 2pm.  The single task would become two separate tasks and the date's time would be shifted to the desired minute of the day.

```ts
    times_in_minutes?: number[];
```

The interval is how many frequency periods to skip when calculating instances.  For example, to schedule something that happens `"every six hours"` you would create a schedule with an `"hourly"` frequency and interval of `6`.

```ts
    interval: number;
```

**This is a custom extension on top of `rrule`**.  Instances is a simple multiplier.  Everytime the above rules would yield an instance we will create duplicate it X number of times.  This is how we accomplish things such as `"Twice Daily"`.

```ts
    instances: number;
```

**This is a custom extension on top of `rrule`**.  Setting the `relative` flag instructs the scheduler to derive the until date based on the period (`daily`, `weekly`, &c).

```ts
    relative: boolean;
}
```

## Exceptions

Because schedules can be infinite (no until date) it is not possible to store information about every single instance that can be created by a schedule.  Because of this the list of instances is calculated in memory every time using the rules provided in the `Schedule` object.

This would work well if we didn't have instances that can be modified the the user (`completed`, `reassigned`, &c).

To solve this problem we store information about specific instances, but only in a few cases:
- The instance has been rescheduled
- The instance has been cancelled
- The instance has been completed
- The instance has been edited (assignee, &c)

This is done via schedule exceptions:

```ts
    interface Exception<T> {
```

The date the original event occurs in the schedule.  The schedule continues to calculate the instance at the original date before exceptions are applied.

```ts
        original_date: Date;
```

This position is the positional index of the task relative to the other instances created by applying the `instances` multiplier.  So a schedule with `instances: 3` would create `3` instances and we might be modifying instance `2` so position would be `2`.

```ts
        position: number;
```

The new date for the instance.  If not provided then the instance continues to ocurr at the regularly scheduled date.  However, setting this date allows us to move the instance to another time.

```ts
        exception_date?: Date;
```

Express that the instance has been cancelled.  It will be removed from the list of instances calculated by the schedule.

```ts
        cancelled?: boolean;
```

Express that the event has been completed.

```ts
        completed?: boolean;
```

Any instance data that is meant to override the same property in the template.  Meaning if the parent object has an assignee of `undefined` the task can override that by specifying an assignee of any other value.

```ts
        overrides?: Partial<T>;
    }
```

## Task Instances

A task instance is determined by extrapolating the schedule object and applying exeptions to task instances.  Each resulting object is stuffed into a `Task`.

```ts
class Task {
```

The task address is the information that we need in order to uniquely identify a task.  It contains the id of the `tasklist`, the id of the `schedule`, the `original date`, and `position`.

```ts
    address: TaskID;
```

This is the date at which the task occurs after applying exceptions.  It is potentially different from the `original date` stored in the address.

```ts
    date: Date;
```

This is the date at which the task is due.  Generally it will be calculated by the end of the period that calculated the task e.g., `hourly`, `daily`, etc.

```ts
    due: Date;
```

The `out_of` property directly matches the `instances` value of the schedule that generated this instance.  The `position` is the positional index of the instance.

```ts
    position: number;
    out_of: number;
```

This is a `CareGiver.Reference` that points to the individual assigned to this instance.

```ts
    caregiver: null | CareGiver.IReference;
```

Whether this instance has been completed.

```ts
    completed: boolean;
}
```

## TaskList

A `Tasklist` is an object capable of deriving zero or more `Task Instances` by applying `Exceptions` to zero or more `Schedules`.

```ts
class TaskList {
    _id: database.ObjectID;
    version: string;
```

A TaskList belongs to a particular patient.

```ts
    patient_id: database.ObjectID;
```

A TaskList may be related to a particular medication.

```ts
    medication_id?: database.ObjectID;
```

Defaults is any values that are applied to all calculated instances.  Each instance can override the defaults using `Exceptions`.

```ts
    defaults: ITask;
```

Shared is any information that is shared across all instances of a Task.  For example, notes and attachments are shared in that adding a note/attachment to an instance should reflect the change in all of the other instances.

```ts
    shared: ITaskShared;
```

A `TaskList` may have zero or more schedules.  Each schedule calculates instances which the `TaskList` aggregates to return the list of task instances.  An `EventSequence` is just the object that extrapolates the list of tasks for a given schedule.

```ts
    sequences: Array<EventSequence<ITask>>;
```

The number of tasks in the tasklist that are considered overdue.  This is used when deciding whether to notify a given caregiver based on their notification settings.

```ts
    overdue_count: number;
```

## Rescheduling

As previously mentioned a `TaskList` can contain multiple schedules.  As currently implemented schedules are not allowed to overlap.  When the schedule for a `TaskList` is modified the following happens:

- The currently active schedule ends just before the new schedule would apply
- Any future but inactive schedules are deleted (including exceptions)
- The new schedule is appended to the list ready to activate after the current

Task instances created using the current schedule (and past ones) will continue to be calculated as always, but once the new schedule is active the new task instances will follow the new schedule.