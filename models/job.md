# Job

A `Job` is an object that is scheduled to be run by a worker process at some point in the future.

```ts
export class Job {
```

To create a Job use the factory function.

```ts
    static factory(options: Job.FactoryOptions): ScheduledJob
```

Creates a job using `Job.factory` and then inserts it into the database.

```ts
    static async create(db: database.Db, options: Job.FactoryOptions): Promise<ScheduledJob>
```

Write a job into the database

```ts
    static async insert(db: database.Db, job: ScheduledJob): Promise<ScheduledJob>
```

Search for jobs in the database.  `Job.SearchOptions` ensures that only the desired jobs are dequeued.  When a job is dequeued it is marked as `processing` so that no other processes can find it in the queue.  Each job is given an estimated completion date so that if the process that handles it doesn't acknowledge completion the job can be put back into a `pending` status.

```ts
    static async dequeue(db: database.Db, options: Job.SearchOptions): Promise<ScheduledJob[]>
```

Resets any jobs in the `processing` state whose completion_date has ellapsed back into a `pending` state.  This will happen if a worker fails to acknowledge success or failure of the job.

When a job is reset the `attempts` counter is incremented.  This counter can be used to avoid dequeueing the job again if it leads to too many failures.

```ts
    static async resetIdle(db: database.Db, threshold: number|Date): Promise<void>
```

Workers use this method to mark a job as completed.

```ts
    static async completed(db: database.Db, job: ScheduledJob): Promise<ScheduledJob>
```

This method will mark the job as completed, but also schedule it to run again in the future.  Workers use this method to run a job as a recurring process.

```ts
    static async reschedule(db: database.Db, job: ScheduledJob, date: number|Date): Promise<ScheduledJob>;
```

This method marks the job as failed.  The `attempts` counter is incremented so that the job can be ignored if it fails too many times.

```ts
    static async failed(db: database.Db, job: ScheduledJob, error: Error): Promise<ScheduledJob>
```

Method used to validate a job object prior to insertion into the database.
This method throws if the object does not match the desired schema.

```ts
    static validate(job: ScheduledJob): void
```

Search and Factory Options

```ts
export namespace Job {
    export type FactoryOptions = IScheduledJob & Partial<IJob>;
    export interface SearchOptions {
        date: number|Date;
        type: IJob.Type | IJob.Type[];
        limit: number;
        attempts?: number;
        timeout?: number; // seconds
    }
}
```
