# CareGiver

Each `User` can have zero or one associated `CareGiver` objects.  If the user has a caregiver object then they are able to join patient carecircles and utilize the mobile app.

```ts
class CareGiver implements ICareGiver {
    // Unique identifier
    _id: database.ObjectID;

    // Unique user reference
    user_id: database.ObjectID;

    // Database document version
    version: string;

    // ID of associated PAC (if any)
    pac_id: null | database.ObjectID;

    // Full Name of the caregiver (public to carecircle)
    full_name: string;

    // List of addresses (public to carecircle)
    addresses: IContact.PhysicalAddress[];

    // List of phone numbers (public to carecircle)
    phones: IContact.PhoneNumber[];

    // List of email addresses (public to carecircle)
    emails: IContact.EmailAddress[];

    // List of carecircle invitations
    invites: CareGiver.Invite[];
}
```

## CareCircle Invites

When a CareGiver is invited to join a carecircle an invitation object will be added to their `invites`.  The CareGiver is not yet considered a part of the patient's carecircle and will not have API access to Patient data.

```ts
class Invite {
    // Patient identifier
    _id: database.ObjectID;

    // Patient's name
    full_name: string;

    // Relationship to patient
    relationship: string;

    // Created date
    created: Date;
}
```

If a caregiver accepts the invitation they will be added to the patient's carecircle.

If a caregiver rejects the invitation the invite object is removed from their list and no further action is taken.

## CareGiver References

A CareGiver Reference is the smallest amount of data that allows us to reference a caregiver object from elsewhere.  This is used particularly when assigning a task to a caregiver.

```ts
class Reference {
    _id: database.ObjectID;
    full_name: string;
}
```

## CareGiver Contact

A CareGiver Contact is the combination of caregiver data (contact info) and information that is specific to the caregiver + patient e.g, relationship and notes.

```ts
class Contact {
    _id: database.ObjectID;
    full_name: string;
    photo: Media.Reference | null;
    addresses: IContact.PhysicalAddress[];
    phones: IContact.PhoneNumber[];
    emails: IContact.EmailAddress[];
    relationship: string;
    notes: Note[];
    read_discharge_docs: boolean;
    notifications: Notification.ISettings;
}
```