# Document

The only required field for creating a document instance is `name`.

```ts
export namespace Document {
    export type FactoryOptions<Schema> = Require<Document<Schema>, "name">;
}
```

The document is defined by the following fields:

Name       | Description
---------- | -------------
name       | The document name.  This value is unique across documents
version    | Ensures we are operating on the latest DB document prior to writing to DB
created    | Date that the document was created
updated    | Date that the document was updated
value      | The document body.  Schema for this field is determined by the shape of the `Schema` type

```ts
export class Document<Schema> {
    name: string;
    version: string;
    created: Date;
    updated: Date;
    value: T;
```

Creates an instance of `Document<Schema>`

```ts
    static factory<Schema>(options: Document.FactoryOptions<Schema>): Document<Schema>
```

Gets an instance of `Document<Schema>` from the database.  If the document does not exist it will be inserted.

```ts
    static async get<Schema>(db: database.Db, name: string): Promise<Document<Schema>>
```

Change document fields

```ts
    update(document: Partial<Document<Schema>>): void
```

Write the current document to the database.  Replaces the previous contents of the document.

```ts
    async save(this: Document<Schema>, db: database.Db): Promise<void>
```

Execute an arbitrary mongo replace query.

```ts
    async execute(this: Document<Schema>, db: database.Db, query: any): Promise<void>
```
