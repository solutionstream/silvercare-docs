# MediaLibrary

Each `User` can have zero or one `MediaLibrary`.  The media library is a collection of media files owned by a particular user.

```ts
export namespace Media {
```

## Media Files

A `Media.File` is a record containing metadata about a file stored in s3.

```ts
    export class File {
        // Unique identifier for file
        _id: database.ObjectID;
        // Directory in s3 where the file is stored
        basedir: string;
        // Filename of file in s3
        filename: string;
        // basedir + filename
        filepath: string;
        // Original filename of the file
        original_filename: string;
        // Size in bytes
        size: number;
        // Mimetype string
        mimetype: string;
        // Storage type.  Only supports s3
        storage: Media.Storage;
        // Date the file was created
        created: Date;
        // Date the file was updated
        updated: Date;
        // Arbitrary string tags useful for searching files
        tags: string[];
    }
```

## Media References

A `Media.Reference` is just enough information to uniquely identify a file.  Mainly, it is the id of the user that owns the file and the id of the file itself.  Media references are used to handle attachments throughout the application, allowing multiple users to reference and access files owned by separate users.  Access to another user's files is determined by the API endpoints that serve information about files.

```ts
    class Reference {
        user: database.ObjectID;
        file: database.ObjectID;
    }
```

## Media Library

A media library is just a collection of media files.  `Pending` files are those for which an upload has been authorized but not yet completed. 

```ts
class MediaLibrary implements IMediaLibrary {

    _id: database.ObjectID;
    version: string;
    user_id: database.ObjectID;
    files: Media.File[];
    pending: Media.File[];
```

The media library handles enforcement of file quotas (disabled).

```ts
    space_files(): number;
    space_pending(): number;
    space_total(): number;
    check_quotas(size: number): boolean;
```

And also the special case of finding a user's profile photo (public to all).
The profile photo is the first file containing the `profile` tag.

```ts
    profile_photo(): undefined|Media.File;
    profile_reference(user_id: database.ObjectID): undefined|Media.Reference;
}
```