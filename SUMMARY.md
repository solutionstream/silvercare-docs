# Summary

## Setup
* [Configuration](setup/configuration.md)
* [Building](setup/building.md)
* [Running Tests](setup/testing.md)
* [Initial Setup](setup/initial.md)

## Models
* [Document](models/document.md)
* [Jobs](models/job.md)
* [User](models/user.md)
    * [CareGiver](models/caregiver.md)
    * [DeviceList](models/devicelist.md)
    * [MediaLibrary](models/medialibrary.md)
    * [PAC](models/pac.md)
    * [Patient](models/patient.md)
        * [ActivityLog](models/activitylog.md)
        * [TaskList](models/tasklist.md)

## API
* [Overview](routes/overview.md)
* [Activities](routes/activity_routes.md)
    * [Create Journal](routes/activity_routes.md#create-journal)
    * [Search Activities](routes/activity_routes.md#search-activities)
* [CareGivers](routes/caregiver_routes.md)
    * [Create CareGiver](routes/caregiver_routes.md#create-caregiver)
    * [Get CareGiver](routes/caregiver_routes.md#get-caregiver)
    * [Get Patient List](routes/caregiver_routes.md#get-patient-list)
* [DeviceList](routes/device_routes.md)
* [Media](routes/medialibrary_routes.md)
* [Medication](routes/medication_routes.md)
* [PAC](routes/pac_routes.md)
* [Patient](routes/patient_routes.md)
* [TaskList](routes/task_routes.md)
* [User](routes/user_routes.md)

## React Native
* [Overview](react_native/overview.md)
* [Bugs](react_native/bugs.md)