# Configuration

The very first thing that needs to be done is to install a `.env` file.
This file contains all of the configurable settings for the application server.

The project ships with a `.env.sample` file that can be used as a starting point.
Edit the file according to your needs and environment.

### Database
This section contains the settings for establishing a database connection.
The application server uses separate database configurations for the application vs. unit tests.

In order for unit tests to run in isolation each test will create a database and drop it at the end.
For this reason, please ensure that the database user for unit tests has permission to create/drop databases.

```ini
DB_HOST="aws-us-west-2-portal.2.dblayer.com:15995"
DB_NAME="silvercare"
DB_USER="silvercare"
DB_PASS="Z9ByxY7gc"
DB_SSL=TRUE 

DB_TEST_HOST="localhost:27017"
DB_TEST_NAME="test"
DB_TEST_USER=
DB_TEST_PASS=
DB_TEST_SSL=
```

### Authentication
Settings for password encryption and account lockout.

```ini
BCRYPT_ROUNDS=10
FAILED_LOGINS_MAX=10
ACCOUNT_LOCKOUT_MINS=45
```

### AWS
This section stores the settings for using AWS Services

```ini
S3_BUCKET="dev-silvercare.silvervue.com"
S3_REGION="us-west-1"
AWS_DEFAULT_REGION="us-west-2"
AWS_ACCESS_KEY="AKIAIFAPSTLEYNDMWLBA"
AWS_SECRET_KEY="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"
```

### Dashboard
This section stores dashboard settings.  The base url is used to create links when sending emails to users.

```ini
DASHBOARD_BASE_URL="http://localhost/#"
```

### Email
This section stored email settings.  The from address is used when sending email to users.

```ini
FROM_EMAIL="info@silvervue.com"
```

### FreeUs
This section contains settings required for communicating with the FreeUS API.

```ini
FREEUS_ENDPOINT="http://uat2.freeus.com/webapi/"
FREEUS_API_KEY="UATPassword1"
```

### Media
This section keeps settings relating to media management.

```ini
PER_FILE_QUOTA=0 #  Maximum file size a user can upload (0 means disabled)
PER_USER_QUOTE=0 #  Maximum total space a user can use (0 means disabled)
```

### Notifiations
This section contains settings relating to the timing of notifications
```ini
OVERDUE_AFTER_MINS=60
REMIND_BEFORE_MINS=30
```

### Session
This section contains settings related to signing authentication tokens

```ini
SESSION_SECRET="63CA8A7C-25EB-4A5A-BB92-7340C23E35C5"
SESSION_ISSUER="SilverCare Server"
```

### SUPER USER
This section contains settings related to the super user.

```ini
SUPER_USER_EMAIL="olopez@solutionstream.com"
```