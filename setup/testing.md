# Running Testsj

### MongoDB
To run the tests you will need to have a running mongodb server.  The unit tests create and drop database to ensure test isolation, so make sure that the mongo user has permission to create/drop databases.

Configure the mongodb details in the `.env` file.

### Dependencies
The environment that runs the tests needs to have all of the development dependencies.

`$ yarn install --development`

### Test Runner
Running the tests is as simple as:

`$ npm run test:runner`

### Test Coverage
Instrumenting the tests to collect test coverage is also easy:

`$ npm run test:coverage`

Test coverage will be written to the `coverage` directory.