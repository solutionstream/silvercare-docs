# Initial Setup

## Super User
Every time the application server is launched it will look for a super user account matching the `.env` configuration.

If the account is found and does not contain a password an email will be sent to the configured super user account.

The link directs to the dashboard with a JWT token that allows setting a super user password.

If the dashboard cannot be run setup can be run programatically by calling the update user api endpoint with the API token

```
POST /user/<user_id> HTTP/1.1
Host: localhost:8000
Content-Type: application/json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0Mjc1ZGExNC1iZjY0LTRmZDItYjU1Yy02NjU1ZGRiMmQ0NmQiLCJpc3MiOiJTaWx2ZXJDYXJlIFNlcnZlciIsInN1YiI6InBhY21hbkBzdXBlcmx1bWluYWwubWUiLCJhdWQiOiIqIiwiZXhwIjoxNTAyNzA4NTc2LCJpYXQiOjE1MDE2OTcyNzZ9.jx6WhrpQJHtFJXVTMbRgkvFElEs-fSYcJi_M3wP7ALU

{
    "password": "supersecretpassword"
}
```

That's it!  Use the super user and admin dashboard to setup any additional accounts.