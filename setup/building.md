# Building The Server

The application server is writtent to run within docker containers, this enables development and production to run in identical environments.

To build the docker images you will need to install Docker and Docker Compose for your operating system.

### Builder

The builder container will install development dependencies and perform necessary build steps to get the server code.  The code is built into a docker volume that can be access by the other images.  This step must be run any time the application code changes.

To build for development:
`$ NODE_ENV=development docker-compose up --build builder`

To run for production:
`$ NODE_ENV=production docker-compose up --build builder`

### Server

The server container only installs the production dependencies which makes it adequate for deployment.

To build for development:
`$ NODE_ENV=development docker-compose up --build server`

To build for production:
`$ NODE_ENV=production docker-compose up --build server`

### Workers

The application server uses worker processes to perform scheduled tasks (somewhat like cron).
The workers container ensures that the correct number of processes of each type is launched and restarts the processes if they crash.

It is possible to run the application server without the workers container during development, but scheduled tasks will not execute.

To build:
`$ docker-compose up --build workers`