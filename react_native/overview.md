## Versions

Node: 7.6.0
Expo: "v18"  
React Native (0.45): "https://github.com/expo/react-native/archive/sdk-18.0.1.tar.gz"

This application was built based on the Expo documentation supporting advanced functionality within React Native. Per the Expo release upgrade process was a notification to start using a specific version of React Native. https://blog.expo.io/expo-sdk-v18-0-0-is-now-available-38e62305283 

## Setup

This application was developed utilizing the tools from Expo (https://expo.io/tools). The app can be run from the command line utilizing instructions from React Native's Create React Native App instructions (https://facebook.github.io/react-native/docs/getting-started.html).

Run on local machine:
    'git clone projectUrl' Clone project into folder. (Install git if it isn't already installed https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
    'npm install' from terminal in folder (Install NodeJS if not already installed. https://nodejs.org/en/)
    Run the Expo app and select Project -> Open project and navigate to where the project was downloaded to.
    If on mac you can run the ios simulator if you have Xcode installed by clicking Device and then the simulator.
    There is also a simulator for Android that can be ran.

On Phone:
    After following previous steps download the Expo app on either android or ios app stores.
    In the file config/api.config.js change the API_URL to the IP address that your computer is on.
    Make sure your phone and computer running the Expo app are on the same network.
    With the Expo app use the scanner to scan the QR code that shows on the computer app when you click the share button.
    
## Features Unfinished

- Discharge Acceptance: The api has been tested through the app to make the call. The reference for displaying them is not fully implemented yet.
- Image loading from S3: The images are able to be loaded throughout the app. The component has a bit left to be finished (extra information before file is submitted to S3). Then implemented throughout the app on the different pages.
- Notes: The Note component is functional and needs to be implemented across the app.
- Medication: Some changes in data layout requires some modifications to the remaining data being displayed. All API calls implemented.
- Styling: There are several global styles remaining to be added to the application.
- Task List: All tasks related to a patient will show, but there is missing the collapse functionality of multiple tasks that are on a daily/weekly series.
- Summary Page: Filtering not setup and styling remains to be handled.