## Known Bugs

- There is a bug that causes a Screen to load twice. The solution most likely lies within the Navigation Library.
- The most recent update version left several non blocking errors in the console. Most of the changes are just deprecation modifications remaining.
- Navigation is still not in a fully stable build and changes keep happening as features are released. Due to this app-nav.routes.js contains the full MainNavigator and is functioning as a Routing component. A custom Nav was implemented and should be dropped into any screen that needs access to it.
- Video Upload does not have a prebuilt video gallery viewer within either Expo or React native.
- Edit functionality is supported, but had a bug with loading. The Add Components need to have their state updated with data from the List Components.
- List doesn't refresh upon submit Add/Edit, the state on the List component does not call the update so one needs to be called.
- An issue where on the Task List certain tasks are not able to be marked as complete.
- Add Medication/Journal Entries was demoed at working state. A change was made that caused the add functionality to fail.

## Rewrites/Optimizations

ListView would not provide necessary functionality required by the app without significant customization in version 0.44 of React Native.
We implemented a version of ScrollView that has desired functionality, but would not be resource efficient as there were larger lists needed.
Currently there is a working example within the Media Gallery of a ListView implementation that will just need to replace the ScrollViews within other components.
   
