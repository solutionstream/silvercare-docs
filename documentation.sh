#!/bin/bash

if ! [ -x "$(command -v gitbook)" ]; then
    npm install -g gitbook-cli
fi

gitbook install
gitbook serve